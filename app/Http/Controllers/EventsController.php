<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DateTime;
use DatePeriod;
use DateInterval;

use Calendar;
use App\Events;

class EventsController extends Controller
{
    public function index() {
        $events = Events::get();
        $event_list = [];

        // $background_colors = array('#f44336', '#9561e2', '#e91e63', '#9c27b0', '#3f51b5', '#2196f3');


        foreach ($events as $key => $event) {
            // $rand_background = $background_colors[array_rand($background_colors)];
            $event_list[] = Calendar::event(
                'Event ID # '.$event->id,
                true,
                new DateTime($event->start_date),
                new DateTime($event->end_date.' +1 Day'),
                $key,
                [
                    'color' => $event->color,
                    'textColor' => 'white',
                    'description' => $event->weekly == 1 ? 'Weekly payment method' : 'Daily payment method',
                    'userId' => 'User ID '.$event->userId
                    // 'rendering' => 'background',
                    // 'dow' => '[2,4]',
                    // 'ranges' => [
                    //     'start' => 'moment().startOf("week")',
                    //     'end' => 'moment().endOf("week").add(7,"d")'
                    // ]
                ]
            );
        }
        
        // $event_list[] = Calendar::event(
        //     'Event One',
        //     true,
        //     new DateTime('2019-02-14'),
        //     new DateTime('2019-02-16 +1 day'),
        //     1,
        //     [
        //     //     // 'timeZone' => 'local',
        //         'dow' => [ 1 , 5 ],
        //         'ranges' => [
        //             'start' => 'DateTime("2019-01-07")',
        //             'end' => 'DateTime("2019-01-15")'
        //         ],
        //         'description' => 'Sample',
        //     //     'color' => '#9561e2',
        //     //     'textColor' => 'white',
        //     //     // 'rendering' => 'background',
        //     //     // 'dow' => '[2,4]',
        //     //     // 'ranges' => [
        //     //     //     'start' => 'moment().startOf("week")',
        //     //     //     'end' => 'moment().endOf("week").add(7,"d")'
        //     //     // ]
        //     ]
        // );

        // return dd($event_list);

        $calendar_details = Calendar::addEvents($event_list)->setOptions([
            // 'selectable' => true,
            // 'selectHelper' => true,
            // 'editable' => true,
            // 'eventLimitText' => 'more',
            // 'eventLimitClick' => 'function() {
            //     $("[data-toggle="tooltip"]").tooltip({ container: "body", html: true });
            //     return "basicWeek";
            // }',
            // 'eventAfterAllRender' => 'function () {
            //     $("[data-toggle="tooltip"]").tooltip({ container: "body", html: true });
            // }'
            
        ])->setCallbacks([
            'eventLimit' => 4,
            'eventRender' => 'function(event, element) {
                $(element).popover({
                    title: event.userId,
                    content: event.description,
                    trigger: "hover",
                    placement: "top",
                    container: "body"
                });             
            }',
            
            // 'dayRender' => 'function(date,cell) {
            //     var now = moment();
            //     if (now.get("date") == date.get("date")) {
            //         cell.addClass("highlight");
            //         $(".fc-today").css("color", "white");
            //     }
            // }',
            // 'eventRender' => 'function(event, element, view) {
            //     console.log(event.start.format());
            //     return (event.ranges.filter(function(range){
            //         return (event.start.isBefore(range.end) &&
            //                 event.end.isAfter(range.start));
            //     }).length)>0;
            // }',
            // 'events' => 'function( start, end, timezone, callback ){
            //     var events = getEvents(start,end); //this should be a JSON request
                
            //     callback(events);
            // }'
        ]);

        // return dd($calendar_details->script());

        return view('home')->with(compact('calendar_details'));
    }

    public function create(Request $request) {
        $messages = [
            'userId.integer' => 'Please enter a valid User ID',
        ];
        
        $validator = Validator::make($request->all(), [
            // 'event_name' => 'required',
            'userId' => 'required|integer',
            'start_date' => 'required|date',
            // 'end_date' => 'required|date|after_or_equal:start_date',
            'daysPayable' => 'required',
            'frequency' => 'required',
            // 'day' => 'nullable',
        ], $messages);

        if ($validator->fails()) {
            // return dd($validator);
            return redirect('home')->withInput()->withErrors($validator);
        }

        /**
         * This method computes for the weekly basis of loan
         */
        if ($request['frequency'] == 0) {
            $event = new Events;
            $event->weekly = $request['frequency'] == 1 ? true : false;
            $event->start_date = $request['start_date'];
            $event->end_date = new DateTime($request['start_date'].'+'.$request['daysPayable'].'day');
            $event->daysPayable = $request['daysPayable'];
            $event->userId = $request['userId'];
            $event->color = $request['color'];
            // return dd($event);
            $event->save();
            return redirect('home')->with('success', 'Data Added');
        } else {
            $start = new DateTime($request['start_date']);
            $interval = new DateInterval('P7D');
            $end = new DateTime($request['start_date'].'+'.$request['daysPayable'].'day');
            $period = new DatePeriod($start, $interval, $end);
            foreach ($period as $key => $date) {
                // echo $date->format('Y-m-d')."<br>";
                // echo $key."<br>";
                // echo $date->format('Y-m-d')."<br>";
                // $event[] = new Events(array(
                //     'weekly' => $request['frequency'],
                //     'start_date' => $date->format('Y-m-d'),
                //     'end_date' => $date->format('Y-m-d'),
                //     'daysPayable' => $request['daysPayable'],
                //     'userId' => $request['userId']
                // ));
                $key = new Events;
                $key->weekly = $request['frequency'] == 1 ? true : false;
                $key->start_date = $date->format('Y-m-d');
                $key->end_date = $date->format('Y-m-d');
                $key->daysPayable = $request['daysPayable'];
                $key->userId = $request['userId'];
                $key->color = $request['color'];
                $key->save();
            }
            // $event->save();
            // return dd($key, $period);
            return redirect('home')->with('success', 'Data Added');
        }

        // $start = new DateTime($request['start_date']);
        // $interval = new DateInterval('P7D');
        // $end = new DateTime($request['start_date'].'+'.$request['daysPayable'].'day');
        // $period = new DatePeriod($start, $interval, $end);
        // foreach ($period as $date) {
        //     echo $date->format('Y-m-d')."<br>";
        // }

        // $endDate = new dateTime($request['start_date'].'+'.$request['daysPayable'].'day');
        // return dd($end);

        // $event = new Events;
        // // $event->event_name = $request['event_name'];
        // $event->start_date = $request['start_date'];
        // $event->end_date = $endDate;
        // $event->daysPayable = $request['daysPayable'];
        // $event->weekly = $request['frequency'] == 1 ? true : false;
        // $event->dayOfWeek = $request['dayOfWeek'] == null ? null : $request['dayOfWeek'];
        // $event->userId = $request['userId'];

        // return dd($event);
        // $event->save();

        // return redirect('home')->with('success', 'Data Added');

    }
}
