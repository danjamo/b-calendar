@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-12">
            @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {{ Session::get('success') }}
            </div>
            @elseif (Session::has('warning'))
            <div class="alert alert-warning" role="alert">
                {{ Session::get('warning') }}
            </div>
            @endif
        </div>
        {{-- <div class="col">
            <div class="card">
                <div class="card-header">Calendar Events</div>
                <div class="card-body">
                        </div>
                        <div class="form-group row">

                            <div class="col-md-3">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="end_date">End Date</label>

                            <div class="col-md-3">
                                <input id="end_date" type="date" class="form-control{{ $errors->has('end_date') ? ' is-invalid' : '' }}" name="end_date" value="{{ old('end_date') }}" required autofocus>

                                @if ($errors->has('end_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('end_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="end_date">Repeating</label>

                            <div class="col-md-3">
                                <select id="frequency" class="form-control{{ $errors->has('frequency') ? ' is-invalid' : '' }}" name="frequency" value="{{ old('frequency') }}" required autofocus>
                                    <option value="0" selected>Daily</option>
                                    <option value="1">Weekly</option>
                                    
                                </select>

                                @if ($errors->has('frequency'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('frequency') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-3">
                                <label for="end_date">Repeat Every</label>
                                <select id="day" class="form-control{{ $errors->has('day') ? ' is-invalid' : '' }}" name="day" value="{{ old('day') }}" required autofocus>
                                    <option value="0" selected>Daily</option>
                                    <option value="1">Weekly</option>
                                    
                                </select>
                                @if ($errors->has('day'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('day') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div> --}}
    </div>

    <div class="row mt-3">
        <div class="col-lg-5">
            <div class="card">
                <div class="card-header">Add a new schedule</div>
                <div class="card-body">
                    <form action="/home" method="POST">
                        @csrf
                        {{-- <div class="form-group">
                            <label for="event_name">Event Name</label>
                            <input id="event_name" type="text" class="form-control{{ $errors->has('event_name') ? ' is-invalid' : '' }}" name="event_name" value="{{ old('event_name') }}" required autofocus>
                            @if ($errors->has('event_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('event_name') }}</strong>
                                </span>
                            @endif
                        </div> --}}
                        <div class="form-group">
                            <label for="userId">User ID</label>
                            <input id="userId" type="text" class="form-control{{ $errors->has('userId') ? ' is-invalid' : '' }}" name="userId" value="{{ old('userId') }}" required autofocus>
                            @if ($errors->has('userId'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('userId') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="frequency">Repeat</label>
                            <select id="frequency" class="form-control{{ $errors->has('frequency') ? ' is-invalid' : '' }}" name="frequency" value="{{ old('frequency') }}" required autofocus>
                                <option value="0" {{ old('frequency') == 0 ? 'selected' : '' }}>Daily</option>
                                <option value="1" {{ old('frequency') == 1 ? 'selected' : '' }}>Weekly</option>
                            </select>
                            @if ($errors->has('frequency'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('frequency') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="daysPayable">Days Payable</label>
                            <input id="daysPayable" type="number" class="form-control{{ $errors->has('daysPayable') ? ' is-invalid' : '' }}" name="daysPayable" value="{{ old('daysPayable') }}" required autofocus>
                            @if ($errors->has('daysPayable'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('daysPayable') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="start_date">Start Date</label>
                            <input id="start_date" type="date" class="form-control{{ $errors->has('start_date') ? ' is-invalid' : '' }}" name="start_date" value="{{ old('start_date') }}" min="{{ date('Y-m-d') }}" required autofocus>
                            @if ($errors->has('start_date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('start_date') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="color">Color</label>
                            <select name="color" id="color" class="form-control">
                                <option value="#9561e2">Navy Purple</option>
                                <option value="#3F51B5">Violet Blue</option>
                                <option value="#F66D9B">Pink</option>
                                <option value="#f44336">Vermilion</option>
                                <option value="#e91e63">Razzmatazz</option>
                                <option value="#4caf50">Green</option>
                            </select>
                        </div>
                        {{-- <div class="form-group">
                            <label for="end_date">End Date</label>
                            <input id="end_date" type="date" class="form-control{{ $errors->has('end_date') ? ' is-invalid' : '' }}" name="end_date" value="{{ old('end_date') }}" min="{{ date('Y-m-d') }}" required autofocus>
                            @if ($errors->has('end_date'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('end_date') }}</strong>
                                </span>
                            @endif
                            <small id="frequency-note"><b class="text-primary">End date</b> must be <b class="text-primary">after or the same</b> as the <b class="text-primary">day of the week</b> to avoid unintended input</small>
                        </div> --}}
                        {{-- <div class="form-group" id="recurring-events">
                            <label for="day">Repeat every (day of the week)</label>
                            <select id="day" class="form-control{{ $errors->has('day') ? ' is-invalid' : '' }}" name="day" value="{{ old('day') }}" required autofocus>
                                <option selected hidden value=null>-- Select Day --</option>
                                <option value="0">Sunday</option>
                                <option value="1">Monday</option>
                                <option value="2">Tuesday</option>
                                <option value="3">Wednesday</option>
                                <option value="4">Thursday</option>
                                <option value="5">Friday</option>
                                <option value="6">Saturday</option>
                            </select>
                            @if ($errors->has('day'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('day') }}</strong>
                                </span>
                            @endif
                        </div> --}}
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg">
            <div class="card">
                <div class="card-header">Calendar</div>
                <div class="card-body">
                    {!! $calendar_details->calendar() !!}
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>

    {!! $calendar_details->script() !!}
</div>

@endsection
